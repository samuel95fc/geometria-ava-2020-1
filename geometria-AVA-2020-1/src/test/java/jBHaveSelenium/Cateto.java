package jBHaveSelenium;

import static org.junit.Assert.*;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.AfterClass;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

@TestInstance(Lifecycle.PER_CLASS)
public class Cateto {

	private static WebDriver driver;

	@AfterClass
	public void close() {
		driver.quit();
	}


	@Given("funcionalidade calculo triangulos")
	public void funcaoCalcular() {
		String path = "C:\\Users\\Samuel Jesus\\eclipse-workspace\\geometria-AVA-2020-1\\triangulo.html";
		driver.get(path);
	}

	@When("selecionar tipo calculo cateto")
	public void calcularHipotenusa() throws InterruptedException {
		driver.findElement(By.id("tipoCalculoSelect")).click();
		Thread.sleep(500);
		driver.findElement(By.id("tipoCalculoSelect")).sendKeys("cateto");
		Thread.sleep(500);
		driver.findElement(By.id("tipoCalculoSelect")).sendKeys(Keys.ENTER);
	}

	@When("informar 6 para $cateto1")
	public void valorCateto1() throws InterruptedException {
		driver.findElement(By.id("cateto1")).sendKeys("6"+ Keys.ENTER);
		Thread.sleep(500);
	}

	@When("informar 10 para $hipotenusa")
	public void valorHipotenusa() throws InterruptedException {
		driver.findElement(By.id("hipotenusa")).sendKeys("10"+ Keys.ENTER);
		Thread.sleep(500);
	}

	@When("solicitar calculo realizado")
	public void calcular() throws InterruptedException {
		driver.findElement(By.id("calcularBtn")).click();
		Thread.sleep(500);
	}

	@Then("cateto2 calculado sera 8")
	public void resultado() throws InterruptedException {
		int valorEsperado = 8;
		Thread.sleep(500);
		assertEquals(valorEsperado, driver.findElement(By.id("cateto2")).getAttribute("value"));
	}
}
