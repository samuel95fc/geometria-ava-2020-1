package jBHaveSelenium;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumTest {
	
	private static WebDriver driver;
	
	@BeforeAll
	public static void geometriaChromeDriver() {
		/*System.setProperty("webdriver.gecko.driver",
				"C:/Users/Samuel Jesus/OneDrive/Documentos/BES/BANCO DE DADOS/geckodriver-v0.26.0-win64/geckodriver.exe");
		driver = new FirefoxDriver();
		*/
		System.setProperty("webdriver.chrome.driver",
				"C:/Users/Samuel Jesus/OneDrive/Documentos/BES/BANCO DE DADOS/chromedriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	@Test
	public void testeHipotenusa() throws InterruptedException {
			driver.get("C:\\\\Users\\\\Samuel Jesus\\\\eclipse-workspace\\\\geometria-AVA-2020-1\\\\triangulo.html");

			Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
			select.selectByVisibleText("Hipotenusa");
			
			WebElement inputCateto1 = driver.findElement(By.id("cateto1"));
			inputCateto1.sendKeys("3" + Keys.ENTER);
			
			WebElement inputCateto2 = driver.findElement(By.id("cateto2"));
			inputCateto2.sendKeys("4" + Keys.ENTER);
			
			WebElement btnClick = driver.findElement(By.id("calcularBtn"));
			btnClick.click();
			
			String value = driver.findElement(By.id("hipotenusa")).getAttribute("value");
			Assertions.assertEquals(value, "5");
		}
	
	@Test
	public void testarCateto() throws InterruptedException {
			driver.get("C:\\\\Users\\\\Samuel Jesus\\\\eclipse-workspace\\\\geometria-AVA-2020-1\\\\triangulo.html");

			Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
			select.selectByVisibleText("Cateto");
			
			WebElement inputCateto1 = driver.findElement(By.id("cateto1"));
			inputCateto1.sendKeys("6" + Keys.ENTER);
			
			WebElement inputHipotenusa = driver.findElement(By.id("hipotenusa"));
			inputHipotenusa.sendKeys("10" + Keys.ENTER);
			
			WebElement btnClick = driver.findElement(By.id("calcularBtn"));
			btnClick.click();
			
			String value = driver.findElement(By.id("cateto2")).getAttribute("value");
			Assertions.assertEquals(value, "8");
		}
}
