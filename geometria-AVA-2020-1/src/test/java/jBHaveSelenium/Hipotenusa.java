package jBHaveSelenium;

import static org.junit.Assert.*;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.AfterClass;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

@TestInstance(Lifecycle.PER_CLASS)
public class Hipotenusa {

	private static WebDriver driver;
	
	@AfterClass
	public void close() {
		driver.quit();
	}
	

	@Given("funcionalidade calculo triangulos")
	public void funcaoCalcular() {
		String path = "C:\\Users\\Samuel Jesus\\eclipse-workspace\\geometria-AVA-2020-1\\triangulo.html";
		driver.get(path);
	}

	@When("selecionar tipo calculo hipotenusa")
	public void calcularHipotenusa() {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Hipotenusa");
	}

	@When("informar 3 para $cateto1")
	public void valorCateto1() {
		WebElement inputValorCateto1 = driver.findElement(By.id("cateto1"));
		inputValorCateto1.sendKeys("3");
	}

	@When("informar 4 para $cateto2")
	public void valorCateto2() {
		WebElement inputValorCateto2 = driver.findElement(By.id("cateto2"));
		inputValorCateto2.sendKeys("4");
	}

	@When("solicitar calculo realizado")
	public void calcular() {
		WebElement buttonCalcular = driver.findElement(By.id("calcularBtn"));
		buttonCalcular.click();
	}

	@Then("hipotenusa calculada sera 5")
	public void resultado() {
		WebElement output = driver.findElement(By.id("hipotenusa"));
		int valorEsperado = 5;
		assertEquals(valorEsperado, output);
	}
}
